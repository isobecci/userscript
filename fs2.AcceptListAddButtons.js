// ==UserScript==
// @name         通販 受注一覧 検索ボタン
// @namespace    http://tampermonkey.net/
// @version      2019.09.04
// @description  try to take over the world!
// @author       You
// @match        https://c19.future-shop.jp/FutureShop2/AcceptList.htm
// @updateURL    https://bitbucket.org/isobecci/userscript/raw/master/fs2.AcceptListAddButtons.js
// @downloadURL  https://bitbucket.org/isobecci/userscript/raw/master/fs2.AcceptListAddButtons.js
// @grant        none
// ==/UserScript==

const DefaultItems = Array.from(document.querySelectorAll('form input, form select')).slice(0,24)
const DefaultValue = DefaultItems.map(item => item.value)
const mainForm = document.querySelector('[name="form"]');
function clearForm() {
  DefaultItems.forEach((e, i) => {
    e.value = ''
  })
}
function today() {
  return new Date().toISOString().replace(/T.+/,'').replace(/-/g, '/')
}
function dayRecent() {
  let forwardDay = new Date().getDay() + 3;
  // if (forwardDay > 2 && forwardDay < 5) {
  //   forwardDay = 1;
  // } else {
  //   forwardDay += 3
  // }
  Date.now() < +new Date('2019-08-24') && Date.now() > +new Date('2019-08-10') && (forwardDay += 7)
  Date.now() < +new Date('2019-12-28') && Date.now() > +new Date('2020-01-05') && (forwardDay += 9)
  return new Date(Date.now() - 24 * 3600 * 1000 * forwardDay).toISOString().replace(/T.+/,'').replace(/-/g, '/')
}
function dayBeforeOneYear() {
  const day = new Date()
  day.setFullYear(day.getFullYear()-1,)
  return day.toISOString().replace(/T.+/,'').replace(/-/g, '/')
}
function dayReset() {
  const day = new Date()
  day.setMonth(day.getMonth()-1,)
  day.setDate(1)
  return day.toISOString().replace(/T.+/,'').replace(/-/g, '/')
}

(function() {
    'use strict';

    // Your code here...
        if (mainForm) {
      const buttons = {
        '未入金（処理済）': function () {
          clearForm();
          DefaultItems[0].value = '01' // 日付: 受注日
          DefaultItems[1].value = dayBeforeOneYear()
          DefaultItems[9].value = '05'; // ステータス: 処理済み
          DefaultItems[13].value = '02'; // 入金状況: 未入金
          document.querySelector('[name="imageField42"]').click();
        },
        '本日発送（処理済）': function () {
          clearForm();
          DefaultItems[0].value = '02'; // 日付: 発送日
          DefaultItems[1].value = today();
          DefaultItems[4].value = today();
          DefaultItems[9].value = '05'; // ステータス: 処理済み
          DefaultItems[14].value = '01'; // 発送状況: 発送済
          document.querySelector('[name="imageField42"]').click();
        },
        '本日発送済（ネコ）': function () {
          clearForm();
          DefaultItems[0].value = '02'; // 日付: 発送日
          DefaultItems[1].value = today();
          DefaultItems[4].value = today();
          // DefaultItems[9].value = '01'; // ステータス: 通常処理
          DefaultItems[12].value = '05'; // 配送サービス: ネコポス
          DefaultItems[13].value = '01'; // 入金状況: 入金済
          DefaultItems[14].value = '01'; // 発送状況: 発送済
          document.querySelector('[name="imageField42"]').click();
        },
        '本日発送済（佐川）': function () {
          clearForm();
          DefaultItems[0].value = '02'; // 日付: 発送日
          DefaultItems[1].value = today();
          DefaultItems[4].value = today();
          // DefaultItems[9].value = '01'; // ステータス: 通常処理
          DefaultItems[12].value = '01'; // 配送サービス: 宅配便
          DefaultItems[14].value = '01'; // 発送状況: 発送済
          document.querySelector('[name="imageField42"]').click();
        },
        '直近の未発送': function () {
          clearForm();
          DefaultItems[0].value = '01'; // 日付: 受注日
          DefaultItems[1].value = dayRecent();
          // DefaultItems[9].value = '01'; // ステータス: 通常処理
          DefaultItems[14].value = '02'; // 発送状況: 未発送
          document.querySelector('[name="imageField42"]').click();
        },
        '直近の未発送（佐川・入金済）': function () {
          clearForm();
          DefaultItems[0].value = '01'; // 日付: 受注日
          DefaultItems[1].value = dayRecent();
          // DefaultItems[9].value = '01'; // ステータス: 通常処理
          DefaultItems[12].value = '01'; // 配送サービス: 宅配便
          DefaultItems[13].value = '01'; // 入金状況: 入金済
          DefaultItems[14].value = '02'; // 発送状況: 未発送
          document.querySelector('[name="imageField42"]').click();
        },
        '直近の未発送（佐川・代引）': function () {
          clearForm();
          DefaultItems[0].value = '01'; // 日付: 受注日
          DefaultItems[1].value = dayRecent();
          // DefaultItems[9].value = '01'; // ステータス: 通常処理
          DefaultItems[11].value = '02'; // 決済種別: 代金引換
          DefaultItems[12].value = '01'; // 配送サービス: 宅配便
          DefaultItems[14].value = '02'; // 発送状況: 未発送
          document.querySelector('[name="imageField42"]').click();
        },
        '直近の未発送（ネコ）': function () {
          clearForm();
          DefaultItems[0].value = '01'; // 日付: 受注日
          DefaultItems[1].value = dayRecent();
          // DefaultItems[9].value = '01'; // ステータス: 通常処理
          DefaultItems[12].value = '05'; // 配送サービス: ネコポス
          DefaultItems[13].value = '01'; // 入金状況: 入金済
          DefaultItems[14].value = '02'; // 発送状況: 未発送
          document.querySelector('[name="imageField42"]').click();
        },
        '保留': function () {
          clearForm();
          DefaultItems[0].value = '01' // 日付: 受注日
          DefaultItems[1].value = dayBeforeOneYear()
          DefaultItems[9].value = '06'; // ステータス: 保留
          document.querySelector('[name="imageField42"]').click();
        },
        '未入金': function () {
          clearForm();
          DefaultItems[0].value = '01' // 日付: 受注日
          DefaultItems[1].value = dayBeforeOneYear()
          DefaultItems[9].value = '01'; // ステータス: 通常処理
          DefaultItems[13].value = '02'; // 入金状況: 未入金
          document.querySelector('[name="imageField42"]').click();
        },
        '注文書発行': function () {
          clearForm();
          DefaultItems[0].value = '01' // 日付: 受注日
          DefaultItems[1].value = dayRecent()
          DefaultItems[9].value = '01'; // ステータス: 通常処理
          document.querySelector('[name="imageField42"]').click();
        },
        'RESET': function () {
          // document.querySelector('[href="/FutureShop2/AcceptListForwardHook.htm"]').click()
          clearForm();
          DefaultItems[0].value = '01' // 日付: 受注日
          DefaultItems[1].value = dayReset()
          document.querySelector('[name="imageField42"]').click();
        }
      }
      Object.keys(buttons).forEach(e => {
        let Button = document.createElement('button');
        Button.innerText = e;
        Button.type = 'button';
        Button.onclick = buttons[e]
        mainForm.prepend(Button, ' ');
      });
    }
})();